SlackBuddies for iOS
==============================================

SlackBuddies is a code exercise requested by Slack as part of the hiring process. This repository contains the source for the app running on the target iOS 9.0. It uses Slack's API to retrieve all members of a particular team, and show to the user their information.

As for the tech side, I used AFNetworking 3.0 for connections, and LGSideMenuController to rapidly create a side slide menu (Where I use to list the users in the team). This exercise could have easily be done without those two libraries.

Feel free to clone it! By far it's not a perfect code, but I am sure someone will find it useful for leaning or researching.
Please, let me know what you guys think =)

Git
=========================

Providing you have a bitbucket account, you can execute the following commands:

    $ git clone https://marcopaivaf@bitbucket.org/marcopaivaf/slackbuddies-ios.git

Prerequisites
=============

Ensure that the following are installed:

* Xcode 7.0 or higher
* ruby or RVM

Setting up for use with CocoaPods
=================================

This project uses [CocoaPods](http://cocoapods.org) - an Objective-C library manager similar to RubyGems. 
It is possible to build the projects without installing Cocoapods, however, if you modify any Cocoapod dependencies, it will have to be installed.

You can see which pods it uses by looking at the `{Repository}/Podfile`.

Here is how you set it up.

If you're using RVM
-------------------

If you are using [RVM](https://rvm.io), make sure that you specify a RVM ruby version to use. If not, skip to the next section.

Make sure you are in your repository folder. If you use

    $ rvm list

you might see something like this

    rvm rubies

       ruby-1.9.3-p484 [ x86_64 ]
       ruby-2.0.0-p353 [ x86_64 ]

Set up a .ruby-version file in your repository folder like so:

    $ echo "ruby-2.0.0-p353" > .ruby-version
    $ cd ..
    $ cd {ClonedRepo}

Running

    $ rvm list

should show you something like this:

    rvm rubies

       ruby-1.9.3-p484 [ x86_64 ]
    => ruby-2.0.0-p353 [ x86_64 ]


Install the CocoaPods gem
-------------------------

Run

    $ [sudo] gem install cocoapods

then you have to execute CocoaPods setup command:

    $ pod setup


Building SlackBuddies
==================

If you change any Cocoapod dependencies, you will have to run the following from the project root:

    $ pod install

to install the pods that the project depends on. When commmiting the changes, be sure to add the files under 
`{Repository}/Pods` to the repository.

Open `{Repository}/SlackBuddies.xcworkspace` in Xcode, select the SlackBuddies build target and build using the Product menu.