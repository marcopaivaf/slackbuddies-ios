//
//  AppDelegate.h
//  SlackBuddies
//
//  Created by Marco Paiva on 2016-05-15.
//  Copyright © 2016 Marco Paiva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@class SKBMenuController;
@class SKBNavigationController;

@interface SKBAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (SKBMenuController *)shareMenuController;
- (SKBNavigationController *)shareNavigationController;

- (BOOL)hasPersistentDataForEntity:(NSString *)entityName;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;


@end

