//
//  SKBCentralViewController.m
//  SlackBuddies
//
//  Created by Marco Paiva on 2016-05-17.
//  Copyright © 2016 Marco Paiva. All rights reserved.
//

#import "SKBCentralViewController.h"
#import "SKBNavigationController.h"
#import "SKBMenuController.h"
#import "SKBSessionManager.h"
#import "SKBAppDelegate.h"
#import "SKBUser.h"
#import <CoreData/CoreData.h>

@interface SKBCentralViewController ()

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property (strong, nonatomic) IBOutlet UIImageView *feedbackImageView;
@property (strong, nonatomic) IBOutlet UILabel *feedbackTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *feedbackDescriptionLabel;

@end

@implementation SKBCentralViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self configureCentralView];
    [self determinateFirstLaunch];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)configureCentralView
{
    self.managedObjectContext = [(SKBAppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    
    UIImage *slackIconNormal = [UIImage imageNamed:@"SlackIcon"];
    UIImage *slackIconSelected = [UIImage imageNamed:@"SlackIcon_Selected"];
    
    UIButton *face = [UIButton buttonWithType:UIButtonTypeCustom];
    face.bounds = CGRectMake(0, 0, slackIconNormal.size.width, slackIconNormal.size.height);
    
    [face setImage:slackIconNormal forState:UIControlStateNormal];
    [face setImage:slackIconSelected forState:UIControlStateHighlighted];
    [face setImage:slackIconSelected forState:UIControlStateDisabled];
    
    [face addTarget:self action:@selector(openLeftView) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:face];
    self.navigationItem.leftBarButtonItem.enabled = NO;
    
    [self.navigationItem setTitleView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"TopBarLogo"]]];
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:45.0f/255.0f green:51.0f/255.0f blue:58.0f/255.0f alpha:1.0f]];
    [self.navigationController.navigationBar setTranslucent:NO];
}

- (void)determinateFirstLaunch
{
    if ([(SKBAppDelegate *)[[UIApplication sharedApplication] delegate] hasPersistentDataForEntity:@"SKBUser"]) {
        [self showWelcomeFeedback];
    } else {
        //Let´s give 2 send before requesing the data. We want to give some time for the user to absorb the messge in the screen, since the data request could be too fast;
        [self performSelector:@selector(requestSlackData) withObject:nil afterDelay:2.0f];
    }
}

- (void)requestSlackData
{
    __weak typeof(self) weakSelf = self;
    [[SKBSessionManager sharedManager] requestSlackUsersWithSuccessCallback:^(NSDictionary *responseObject) {
        if (weakSelf) {
            __strong typeof(weakSelf) strongSelf = weakSelf;
            [strongSelf didSuccessfullyReceiveRequestWithData:responseObject];
        }
    } andFailureCallback:^(NSError *error) {
        if (weakSelf) {
            __strong typeof(weakSelf) strongSelf = weakSelf;
            [strongSelf didFailRequestWithError:error];
        }
    }];
}

- (void)didSuccessfullyReceiveRequestWithData:(NSDictionary *)responseObject
{
    if (responseObject != nil) {
        if ([[responseObject objectForKey:@"ok"] boolValue] == NO) {
            [self showErrorAlert:[[SKBSessionManager sharedManager] translateErrorMessageFromSlackAPI:[responseObject objectForKey:@"error"]]];
        } else {
            NSArray *users = [responseObject objectForKey:@"members"];
            [self insertNewUsers:users];
            
            NSError *saveError = nil;
            if ([self.managedObjectContext save:&saveError] == NO) {
                [self showErrorAlert:[[SKBSessionManager sharedManager] translateErrorMessageFromSlackAPI:@"unknown"]];
            } else {
                [self showSuccessFeedback];
            }
        }
    } else {
        NSLog(@"SKBCentralViewController Error: Success callback from request didn't return a valid NSDictionary.");
        [self showErrorAlert:[[SKBSessionManager sharedManager] translateErrorMessageFromSlackAPI:@"unknown"]];
    }
}

- (void)didFailRequestWithError:(NSError *)error
{
    if (error != nil) {
        [self showErrorAlert:error];
    } else {
        NSLog(@"SKBCentralViewController Error: Failure callback from request didn't return a valid NSError.");
        [self showErrorAlert:[[SKBSessionManager sharedManager] translateErrorMessageFromSlackAPI:@"unknown"]];
    }
}

- (void)insertNewUsers:(NSArray *)newUsers
{
    if (newUsers != nil && newUsers.count > 0) {
        for (NSDictionary *userData in newUsers) {
            if (userData != nil) {
                SKBUser *newUser = [NSEntityDescription insertNewObjectForEntityForName:@"SKBUser" inManagedObjectContext:self.managedObjectContext];
                
                newUser.id = [userData objectForKey:@"id"];
                newUser.username = [userData objectForKey:@"name"];
                newUser.realName = ([userData objectForKey:@"real_name"] != nil) ? [userData objectForKey:@"real_name"] : [[userData objectForKey:@"profile"] objectForKey:@"real_name"];
                newUser.imageURL = [[userData objectForKey:@"profile"] objectForKey:@"image_original"];
                newUser.imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:newUser.imageURL]];
                newUser.title = [[userData objectForKey:@"profile"] objectForKey:@"title"];
            }
        }
    }
}

- (void)showErrorAlert:(NSError *)error
{
    if (error != nil) {
        NSArray *alertActions = nil;
        __weak typeof(self) weakSelf = self;
        
        if (error.code == SKBSlackWithUnknownError || error.code == SKBSlackRequestTimedOutError) {
            
            UIAlertAction *yesAction = [UIAlertAction actionWithTitle:@"Yes"
                                                                style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * _Nonnull action) {
                                                                  if (weakSelf) {
                                                                      __strong typeof(weakSelf) strongSelf = weakSelf;
                                                                      [strongSelf showRetryingConnectionFeedback];
                                                                      [strongSelf performSelector:@selector(requestSlackData) withObject:nil afterDelay:3.0f];
                                                                  }
                                                              }];
            
            UIAlertAction *noAction = [UIAlertAction actionWithTitle:@"No"
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * _Nonnull action) {
                                                                 if (weakSelf) {
                                                                     __strong typeof(weakSelf) strongSelf = weakSelf;
                                                                     [strongSelf showNoConnectionFeedback];
                                                                 }
                                                             }];
            
            alertActions = @[yesAction, noAction];
        } else {
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok"
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * _Nonnull action) {
                                                                 if (weakSelf) {
                                                                     __strong typeof(weakSelf) strongSelf = weakSelf;
                                                                     [strongSelf showWarningFeedback];
                                                                 }
                                                             }];
            
            alertActions = @[okAction];
        }
        
        [(SKBNavigationController *)self.navigationController showAlertWithTitle:error.localizedDescription
                                                                         message:[NSString stringWithFormat:@"%@%@", error.localizedFailureReason, error.localizedRecoverySuggestion]
                                                                      andActions:alertActions];
    } else {
        NSLog(@"SKBCentralViewController Error: Cannot show a alert for a nil error.");
    }
}

- (void)showWelcomeFeedback
{
    [self updateFeedbackWithImage:[UIImage imageNamed:@"SlackCentralIcon"]
                            title:@"Well Hello There!"
                   andDescription:@"Enjoy stalking your slack buddies!\n=)"];
    
    self.navigationItem.leftBarButtonItem.enabled = YES;
    [(SKBAppDelegate *)[[UIApplication sharedApplication] delegate] shareMenuController].leftViewSwipeGestureEnabled = YES;
}

- (void)showNoConnectionFeedback
{
    [self updateFeedbackWithImage:[UIImage imageNamed:@"ConnectionCentralIcon"]
                            title:@"No Connection!"
                   andDescription:@"Well, seams like we can't reach our serves. Please check your connection and try it again!"];
}

- (void)showRetryingConnectionFeedback
{
    [self updateFeedbackWithImage:[UIImage imageNamed:@"ConnectionCentralIcon"]
                            title:@"Keep Your Fingers Crossed!"
                   andDescription:@"Alright, alright, alright!\n Keep your fingers crossed while we try it again..."];
}

- (void)showWarningFeedback
{
    [self updateFeedbackWithImage:[UIImage imageNamed:@"WarningCentralIcon"]
                            title:@"Oops, my bad!"
                   andDescription:@"I messed up with something =/\nCould you let me know, so I can fix it?"];
}

- (void)showSuccessFeedback
{
    [self updateFeedbackWithImage:[UIImage imageNamed:@"SuccessCentralIcon"]
                            title:@"Hoooray! You are all set!"
                   andDescription:@"Stalk your buddies by tapping on the top left button or swipe from the left edge of your screen!"];
    
    self.navigationItem.leftBarButtonItem.enabled = YES;
    [(SKBAppDelegate *)[[UIApplication sharedApplication] delegate] shareMenuController].leftViewSwipeGestureEnabled = YES;
}

- (void)updateFeedbackWithImage:(UIImage *)image title:(NSString *)title andDescription:(NSString *)description
{
    [UIView transitionWithView:self.self.feedbackImageView
                      duration:0.3f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        self.feedbackImageView.image = image;
                    } completion:NULL];
    
    [self.feedbackTitleLabel setText:title];
    [self.feedbackDescriptionLabel setText:description];
}

- (void)openLeftView
{
    [[(SKBAppDelegate *)[[UIApplication sharedApplication] delegate] shareMenuController] showLeftViewAnimated:YES completionHandler:NULL];
}

@end
