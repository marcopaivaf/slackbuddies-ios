//
//  SKBDetailsViewController.m
//  SlackBuddies
//
//  Created by Marco Paiva on 2016-05-18.
//  Copyright © 2016 Marco Paiva. All rights reserved.
//

#import "SKBDetailsViewController.h"
#import "SKBMenuController.h"
#import "SKBAppDelegate.h"
#import "SKBUser.h"

@interface SKBDetailsViewController ()

@property (nonatomic, strong) SKBUser *user;

@property (nonatomic, strong) IBOutlet UIView *detailsContainerView;
@property (nonatomic, strong) IBOutlet UIView *pictureFrameView;
@property (nonatomic, strong) IBOutlet UIImageView *pictureView;

@property (nonatomic, strong) IBOutlet UILabel *realNameLabel;
@property (nonatomic, strong) IBOutlet UILabel *usernameLabel;
@property (nonatomic, strong) IBOutlet UILabel *titleLabel;

@end

@implementation SKBDetailsViewController

- (instancetype)initWithUserData:(SKBUser *)user
{
    self = [super initWithNibName:@"SKBDetailsViewController" bundle:nil];
    if (self) {
        self.user = user;
    }
    
    return  self;
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    [CATransaction begin];
    [CATransaction setValue:[NSNumber numberWithFloat:1.0f] forKey:kCATransactionAnimationDuration];
    self.detailsContainerView.layer.shadowPath = [[UIBezierPath bezierPathWithRect:self.detailsContainerView.bounds] CGPath];
    self.pictureFrameView.layer.shadowPath = [[UIBezierPath bezierPathWithRect:self.pictureFrameView.bounds] CGPath];
    [CATransaction commit];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configureNavigationBar];
    [self configureDetails];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.detailsContainerView.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.detailsContainerView.layer.shadowOpacity = 0.4f;
    self.detailsContainerView.layer.shadowOffset = CGSizeZero;
    self.detailsContainerView.layer.shadowPath = [[UIBezierPath bezierPathWithRect:self.detailsContainerView.bounds] CGPath];
    self.detailsContainerView.layer.shadowRadius = 7;
    
    self.pictureFrameView.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.pictureFrameView.layer.shadowOpacity = 0.3f;
    self.pictureFrameView.layer.shadowOffset = CGSizeZero;
    self.pictureFrameView.layer.shadowPath = [[UIBezierPath bezierPathWithRect:self.pictureFrameView.bounds] CGPath];
    self.pictureFrameView.layer.shadowRadius = 5;
    
    self.detailsContainerView.layer.cornerRadius = 15;
    self.pictureFrameView.layer.cornerRadius = 15;
    self.pictureView.layer.cornerRadius = 15;
    self.pictureView.layer.masksToBounds = YES;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)configureNavigationBar
{
    UIImage *slackIconNormal = [UIImage imageNamed:@"SlackIcon"];
    UIImage *slackIconSelected = [UIImage imageNamed:@"SlackIcon_Selected"];
    
    UIButton *face = [UIButton buttonWithType:UIButtonTypeCustom];
    face.bounds = CGRectMake(0, 0, slackIconNormal.size.width, slackIconNormal.size.height);
    
    [face setImage:slackIconNormal forState:UIControlStateNormal];
    [face setImage:slackIconSelected forState:UIControlStateHighlighted];
    [face setImage:slackIconSelected forState:UIControlStateDisabled];
    
    [face addTarget:self action:@selector(openLeftView) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:face];
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:45.0f/255.0f green:51.0f/255.0f blue:58.0f/255.0f alpha:1.0f]];
    [self.navigationController.navigationBar setTranslucent:NO];
}

- (void)configureDetails
{
    if (self.user.imageData != nil) self.pictureView.image = [UIImage imageWithData:self.user.imageData];
    self.realNameLabel.text = (self.user.realName != nil && self.user.realName.length > 0) ? self.user.realName : @"Not Provided";
    
    self.usernameLabel.text = [NSString stringWithFormat:@"@%@", self.user.username];
    self.titleLabel.text = (self.user.title != nil && self.user.title.length > 0) ? self.user.title : (([self.user.username isEqualToString:@"slackbot"]) ? @"The Slackbot" : @"Title Not Provided");
}

- (void)openLeftView
{
    [[(SKBAppDelegate *)[[UIApplication sharedApplication] delegate] shareMenuController] showLeftViewAnimated:YES completionHandler:NULL];
}

@end
