//
//  SKBLeftMenuViewCell.h
//  SlackBuddies
//
//  Created by Marco Paiva on 2016-05-18.
//  Copyright © 2016 Marco Paiva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SKBLeftMenuViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *label;

@end
