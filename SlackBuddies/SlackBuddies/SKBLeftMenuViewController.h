//
//  SKBLeftSideMenuViewController.h
//  SlackBuddies
//
//  Created by Marco Paiva on 2016-05-17.
//  Copyright © 2016 Marco Paiva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface SKBLeftMenuViewController : UITableViewController <NSFetchedResultsControllerDelegate>

@end
