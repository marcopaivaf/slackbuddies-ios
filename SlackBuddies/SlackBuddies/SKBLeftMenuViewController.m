//
//  SKBLeftSideMenuViewController.m
//  SlackBuddies
//
//  Created by Marco Paiva on 2016-05-17.
//  Copyright © 2016 Marco Paiva. All rights reserved.
//

#import "SKBLeftMenuViewController.h"
#import "SKBLeftMenuViewCell.h"
#import "SKBNavigationController.h"
#import "SKBMenuController.h"
#import "SKBUser.h"
#import "SKBSessionManager.h"
#import "SKBAppDelegate.h"

@interface SKBLeftMenuViewController ()

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end

@implementation SKBLeftMenuViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self configureMenu];
    }
    return self;
}

- (void)configureMenu
{
    self.managedObjectContext = [(SKBAppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = [UIColor colorWithRed:55.0f/255.0f green:61.0f/255.0f blue:77.0f/255.0f alpha:1.0f];
    
    self.clearsSelectionOnViewWillAppear = self.splitViewController.isCollapsed;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"SKBLeftMenuViewCell" bundle:nil] forCellReuseIdentifier:@"SKBLeftMenuViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"SKBLeftMenuViewHeader" bundle:nil] forCellReuseIdentifier:@"SKBLeftMenuViewHeader"];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return [(SKBAppDelegate *)[[UIApplication sharedApplication] delegate] shareNavigationController].navigationBar.frame.size.height;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"SKBLeftMenuViewHeader"];
    return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"SKBLeftMenuViewCell" forIndexPath:indexPath];
    [self configureCell:(SKBLeftMenuViewCell *)cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(SKBLeftMenuViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    SKBUser *user = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.label.text = user.username;
    
    UIView *selectionView = [[UIView alloc] initWithFrame:cell.frame];
    selectionView.backgroundColor = [UIColor colorWithRed:76.0f/255.0f green:150.0f/255.0f blue:137.0f/255.0f alpha:1.0f];
    cell.selectedBackgroundView = selectionView;
    [cell setBackgroundColor:[UIColor clearColor]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[(SKBAppDelegate *)[[UIApplication sharedApplication] delegate] shareNavigationController] showDetailsForUser:[[self fetchedResultsController] objectAtIndexPath:indexPath]];
    [[(SKBAppDelegate *)[[UIApplication sharedApplication] delegate] shareMenuController] hideLeftViewAnimated:YES completionHandler:NULL];
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"SKBUser" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:0];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"username" ascending:YES];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:@"Master"];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller
  didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex
     forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller
   didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath
     forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

@end
