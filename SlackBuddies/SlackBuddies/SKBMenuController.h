//
//  SKBSideMenuController.h
//  SlackBuddies
//
//  Created by Marco Paiva on 2016-05-17.
//  Copyright © 2016 Marco Paiva. All rights reserved.
//

#import <LGSideMenuController/LGSideMenuController.h>

@interface SKBMenuController : LGSideMenuController

- (instancetype)initWithRootViewController:(UIViewController *)rootViewController;

@end
