//
//  SKBSideMenuController.m
//  SlackBuddies
//
//  Created by Marco Paiva on 2016-05-17.
//  Copyright © 2016 Marco Paiva. All rights reserved.
//

#import "SKBMenuController.h"
#import "SKBLeftMenuViewController.h"

@interface SKBMenuController ()

@property (strong, nonatomic) SKBLeftMenuViewController *leftSideMenuViewController;

@end

@implementation SKBMenuController

- (instancetype)initWithRootViewController:(UIViewController *)rootViewController
{
    self = [super initWithRootViewController:rootViewController];
    if (self) {
        [self configureLeftSideMenu];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (void)configureLeftSideMenu
{
    self.leftSideMenuViewController = [[SKBLeftMenuViewController alloc] init];
    
    [self setLeftViewEnabledWithWidth:250.f
                    presentationStyle:LGSideMenuPresentationStyleSlideBelow
                 alwaysVisibleOptions:LGSideMenuAlwaysVisibleOnNone];
    
    self.leftViewSwipeGestureEnabled = NO;
    
    self.leftViewStatusBarStyle = UIStatusBarStyleDefault;
    self.leftViewStatusBarVisibleOptions = LGSideMenuStatusBarVisibleOnAll;
    
    [self.leftView addSubview:self.leftSideMenuViewController.tableView];
}


@end
