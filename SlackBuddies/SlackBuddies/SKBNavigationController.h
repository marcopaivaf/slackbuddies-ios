//
//  SKBNavigationController.h
//  SlackBuddies
//
//  Created by Marco Paiva on 2016-05-17.
//  Copyright © 2016 Marco Paiva. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SKBUser;

@interface SKBNavigationController : UINavigationController

- (void)showAlertWithTitle:(NSString *)title message:(NSString *)message andActions:(NSArray *)actions;
- (void)showDetailsForUser:(SKBUser *)user;

@end
