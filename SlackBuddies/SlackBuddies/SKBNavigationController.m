//
//  SKBNavigationController.m
//  SlackBuddies
//
//  Created by Marco Paiva on 2016-05-17.
//  Copyright © 2016 Marco Paiva. All rights reserved.
//

#import "SKBNavigationController.h"
#import "SKBCentralViewController.h"
#import "SKBDetailsViewController.h"
#import "SKBMenuController.h"
#import "SKBSessionManager.h"
#import "SKBAppDelegate.h"
#import "SKBUser.h"
#import <CoreData/CoreData.h>

@interface SKBNavigationController ()

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end

@implementation SKBNavigationController

- (instancetype)initWithRootViewController:(UIViewController *)rootViewController
{
    self = [super initWithRootViewController:rootViewController];
    if (self) {
        
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configureNavigation];
    [self determinateRequestData];
}

- (void)configureNavigation
{
    self.managedObjectContext = [(SKBAppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (BOOL)prefersStatusBarHidden
{
    return NO;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation
{
    return UIStatusBarAnimationNone;
}

//The navigation controller will only request data after the first launch (After we requested one time at first launch from the SKBCentralViewController)
//We also want to request data from here because SKBCentralViewController will not always be visible after first launch.
//If the user is navigating through the slack's user details, we still want to be able to request data (to update the current data) and alert the user with feedback.
- (void)determinateRequestData
{
    if ([(SKBAppDelegate *)[[UIApplication sharedApplication] delegate] hasPersistentDataForEntity:@"SKBUser"]) {
        //Let's take some time to start requesting updated data.
        [self performSelector:@selector(requestSlackData) withObject:nil afterDelay:10.0f];
    }
}

- (void)requestSlackData
{
    __weak typeof(self) weakSelf = self;
    [[SKBSessionManager sharedManager] requestSlackUsersWithSuccessCallback:^(NSDictionary *responseObject) {
        if (weakSelf) {
            __strong typeof(weakSelf) strongSelf = weakSelf;
            [strongSelf didSuccessfullyReceiveRequestWithData:responseObject];
        }
    } andFailureCallback:^(NSError *error) {
        if (weakSelf) {
            __strong typeof(weakSelf) strongSelf = weakSelf;
            [strongSelf didFailRequestWithError:error];
        }
    }];
}

- (void)didSuccessfullyReceiveRequestWithData:(NSDictionary *)responseObject
{
    if (responseObject != nil) {
        if ([[responseObject objectForKey:@"ok"] boolValue] == YES) {
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
            NSEntityDescription *entity = [NSEntityDescription entityForName:@"SKBUser" inManagedObjectContext:self.managedObjectContext];
            [fetchRequest setEntity:entity];
            [fetchRequest setFetchBatchSize:0];
            
            NSError *error = nil;
            NSArray *currentUsers = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
            NSArray *updatedUsers = [responseObject objectForKey:@"members"];
            
            if (error == nil) {
                [self updateCurrentUsers:currentUsers andUpdatedUsers:updatedUsers];
                [self insertNewUsers:updatedUsers andCurrentUsers:currentUsers];
                
                NSError *saveError = nil;
                if ([self.managedObjectContext save:&saveError] == NO) {
                    NSLog(@"SKBNavigationController Error: New data was available, but we were unbale to sava into CoreData - %@", saveError.localizedDescription);
                }
            } else {
                NSLog(@"SKBNavigationController Error: Tried to fetch data but something whent wrong - %@", error.localizedDescription);
            }
        } else {
            NSLog(@"SKBNavigationController Error: Success callback from request returned a not ok status");
        }
    } else {
        NSLog(@"SKBNavigationController Error: Success callback from request didn't return a valid NSDictionary.");
    }
}

- (void)didFailRequestWithError:(NSError *)error
{
    if (error != nil) {
        NSLog(@"SKBNavigationController Error: Fail to request updated data from API - %@", error.localizedFailureReason);
    } else {
        NSLog(@"SKBNavigationController Error: Failure callback from request didn't return a valid NSError.");
    }
}

- (void)updateCurrentUsers:(NSArray *)currentUsers andUpdatedUsers:(NSArray *)updatedUsers
{
    for (SKBUser *currentUser in currentUsers) {
        BOOL userStillExists = NO;
        for (NSDictionary *updatedUser in updatedUsers) {
            if ([currentUser.id isEqualToString:[updatedUser objectForKey:@"id"]]) {
                userStillExists = YES;
                
                //Lets update this user with the lastest data
                currentUser.username = [updatedUser objectForKey:@"name"];
                currentUser.realName = ([updatedUser objectForKey:@"real_name"] != nil) ? [updatedUser objectForKey:@"real_name"] : [[updatedUser objectForKey:@"profile"] objectForKey:@"real_name"];
                currentUser.title = [[updatedUser objectForKey:@"profile"] objectForKey:@"title"];
                currentUser.imageURL = [[updatedUser objectForKey:@"profile"] objectForKey:@"image_original"];
                currentUser.imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:currentUser.imageURL]];
                break;
            }
        }
        
        //if this user doesn't exist in the new updated data, we delete from CoreData
        if (userStillExists == NO) {
            [self.managedObjectContext deleteObject:currentUser];
        }
    }
}

- (void)insertNewUsers:(NSArray *)updatedUsers andCurrentUsers:(NSArray *)currentUsers
{
    for (NSDictionary *updatedUser in updatedUsers) {
        BOOL isNewUser = YES;
        for (SKBUser *currentUser in currentUsers) {
            if ([currentUser.id isEqualToString:[updatedUser objectForKey:@"id"]]) {
                isNewUser = NO;
                break;
            }
        }
        
        //if this is a new user (One that we don't have in our current data), we create a model for him
        if (isNewUser) {
            SKBUser *newUser = [NSEntityDescription insertNewObjectForEntityForName:@"SKBUser" inManagedObjectContext:self.managedObjectContext];
            newUser.id = [updatedUser objectForKey:@"id"];
            newUser.username = [updatedUser objectForKey:@"name"];
            newUser.realName = ([updatedUser objectForKey:@"real_name"] != nil) ? [updatedUser objectForKey:@"real_name"] : [[updatedUser objectForKey:@"profile"] objectForKey:@"real_name"];
            newUser.title = [[updatedUser objectForKey:@"profile"] objectForKey:@"title"];
            newUser.imageURL = [[updatedUser objectForKey:@"profile"] objectForKey:@"image_original"];
            newUser.imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:newUser.imageURL]];
        }
    }
}

- (void)showAlertWithTitle:(NSString *)title message:(NSString *)message andActions:(NSArray *)actions
{
    if (actions != nil && actions.count > 0) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title
                                                                                 message:message
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        for (UIAlertAction *action in actions) {
            if (action != nil) {
                [alertController addAction:action];
            }
        }
        [self presentViewController:alertController animated:YES completion:NULL];
    } else {
        NSLog(@"SKBNavigationController Error: UIAlertController needs to be provided with at least one action.");
    }
}

- (void)showDetailsForUser:(SKBUser *)user
{
    SKBDetailsViewController *detailViewController = [[SKBDetailsViewController alloc] initWithUserData:user];
    
    [self setViewControllers:@[detailViewController]];
    [self popToRootViewControllerAnimated:YES];
}


@end
