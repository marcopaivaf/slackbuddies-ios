//
//  SKBSessionErrors.h
//  SlackBuddies
//
//  Created by Marco Paiva on 2016-05-19.
//  Copyright © 2016 Marco Paiva. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const SKBSessionErrorDomain;

NS_ENUM(NSInteger) {
    SKBSlackNotAuthedError = 500,
    SKBSlackWithInvalidAuthError = 501,
    SKBSlackWithAccountInactiveError = 502,
    SKBSlackRequestTimedOutError = 503,
    SKBSlackWithUnknownError = 504
};