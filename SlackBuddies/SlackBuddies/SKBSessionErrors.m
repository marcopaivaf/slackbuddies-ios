//
//  SKBSessionErrors.m
//  SlackBuddies
//
//  Created by Marco Paiva on 2016-05-19.
//  Copyright © 2016 Marco Paiva. All rights reserved.
//

#import "SKBSessionErrors.h"

NSString *const SKBSessionErrorDomain = @"SKBSessionErrorDomain";