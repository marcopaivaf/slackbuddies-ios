//
//  SKBSessionManager.h
//  SlackBuddies
//
//  Created by Marco Paiva on 2016-05-16.
//  Copyright © 2016 Marco Paiva. All rights reserved.
//

#import "SKBSessionErrors.h"
#import <AFNetworking/AFNetworking.h>

@interface SKBSessionManager : AFHTTPSessionManager

+ (instancetype)sharedManager;

- (void)requestSlackUsersWithSuccessCallback:(void (^)(NSDictionary *responseObject))successCallback
                          andFailureCallback:(void (^)(NSError *error))failureCallback;

- (NSError *)translateErrorMessageFromSlackAPI:(NSString *)errorMessege;

@end
