//
//  SKBSessionManager.m
//  SlackBuddies
//
//  Created by Marco Paiva on 2016-05-16.
//  Copyright © 2016 Marco Paiva. All rights reserved.
//

#import "SKBSessionManager.h"


#define SKB_SLACK_API_BASE_URL      @"https://slack.com/api/"
#define SKB_SLACK_API_USERS_URL     @"users.list?token=%@"

#define SKB_SLACK_PROD_TOKEN        @"xoxp-4698769766-4698769768-18910479235-8fa82d53b2" //if we had a token for production and another for development
#define SKB_SLACK_DEV_TOKEN         @"xoxp-4698769766-4698769768-18910479235-8fa82d53b2"

#ifdef DEBUG
    #define SKB_SLACK_TOKEN         SKB_SLACK_DEV_TOKEN
#else
    #define SKB_SLACK_TOKEN         SKB_SLACK_PROD_TOKEN
#endif


@implementation SKBSessionManager

+ (instancetype)sharedManager
{
    static SKBSessionManager *sessionManager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sessionManager = [[SKBSessionManager alloc] init];
    });
    
    return sessionManager;
}

- (instancetype)init
{
    self = [super initWithBaseURL:[NSURL URLWithString:SKB_SLACK_API_BASE_URL]];
    if (self) {
        [self configureManager];
    }
    
    return  self;
}

- (void)configureManager
{
    self.responseSerializer = [AFJSONResponseSerializer serializer];
    self.requestSerializer = [AFJSONRequestSerializer serializer];
}

- (void)requestSlackUsersWithSuccessCallback:(void (^)(NSDictionary *responseObject))successCallback
                          andFailureCallback:(void (^)(NSError *error))failureCallback
{
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:SKB_SLACK_API_USERS_URL, SKB_SLACK_TOKEN]];
    
    //AFNetwork will dispatch these blocks on the main thread
    [self POST:URL.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        if (successCallback) {
            successCallback(responseObject);
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        if (failureCallback) {
            failureCallback([self translateErrorMessageFromSlackAPI:@"unknown"]);
        }
    }];
}

- (NSError *)translateErrorMessageFromSlackAPI:(NSString *)errorMessege
{
    if (errorMessege == nil || [errorMessege length] == 0) {
        return nil;
    } else {
        NSDictionary *userInfo = nil;
        if ([errorMessege isEqualToString:@"not_authed"]) {
            userInfo = @{NSLocalizedDescriptionKey:NSLocalizedString(@"Oops, that was embarrassing!", nil),
                         NSLocalizedFailureReasonErrorKey:NSLocalizedString(@"We forgot to provide a token.", nil),
                         NSLocalizedRecoverySuggestionErrorKey:NSLocalizedString(@"\nPlease contact this developer for more information.", nil)};
            
            return [NSError errorWithDomain:SKBSessionErrorDomain code:SKBSlackNotAuthedError userInfo:userInfo];
            
        } else if ([errorMessege isEqualToString:@"invalid_auth"]) {
            userInfo = @{NSLocalizedDescriptionKey:NSLocalizedString(@"Oops, that was embarrassing!", nil),
                         NSLocalizedFailureReasonErrorKey:NSLocalizedString(@"Our authentication token is invalid.", nil),
                         NSLocalizedRecoverySuggestionErrorKey:NSLocalizedString(@"\nPlease contact this developer for more information.", nil)};
            
            return [NSError errorWithDomain:SKBSessionErrorDomain code:SKBSlackWithInvalidAuthError userInfo:userInfo];
            
        } else if ([errorMessege isEqualToString:@"account_inactiv"]) {
            userInfo = @{NSLocalizedDescriptionKey:NSLocalizedString(@"Are you a ghost?", nil),
                         NSLocalizedFailureReasonErrorKey:NSLocalizedString(@"We are using a token that corresponds to a deleted user or team.", nil),
                         NSLocalizedRecoverySuggestionErrorKey:NSLocalizedString(@"\nPlease contact this developer for more information.", nil)};
            
            return [NSError errorWithDomain:SKBSessionErrorDomain code:SKBSlackWithAccountInactiveError userInfo:userInfo];
            
        } else if ([errorMessege isEqualToString:@"request_timeout"]) {
            userInfo = @{NSLocalizedDescriptionKey:NSLocalizedString(@"We ran out of time!", nil),
                         NSLocalizedFailureReasonErrorKey:NSLocalizedString(@"We couldn't complete our request. There could be an issue with our servers or your internet connection.", nil),
                         NSLocalizedRecoverySuggestionErrorKey:NSLocalizedString(@"\nDo you wish to try again?", nil)};
            
            return [NSError errorWithDomain:SKBSessionErrorDomain code:SKBSlackRequestTimedOutError userInfo:userInfo];
            
        } else {
            userInfo = @{NSLocalizedDescriptionKey:NSLocalizedString(@"Oops, that was embarrassing!", nil),
                         NSLocalizedFailureReasonErrorKey:NSLocalizedString(@"Something went wrong, and we are not quite sure what it is =/", nil),
                         NSLocalizedRecoverySuggestionErrorKey:NSLocalizedString(@"\nPlease check your connection. Do you wish to try again?", nil)};
            
            return [NSError errorWithDomain:SKBSessionErrorDomain code:SKBSlackWithUnknownError userInfo:userInfo];
        }
    }
}

@end
