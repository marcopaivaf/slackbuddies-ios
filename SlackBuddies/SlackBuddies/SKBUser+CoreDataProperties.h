//
//  SKBUser+CoreDataProperties.h
//  SlackBuddies
//
//  Created by Marco Paiva on 2016-05-19.
//  Copyright © 2016 Marco Paiva. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "SKBUser.h"

NS_ASSUME_NONNULL_BEGIN

@interface SKBUser (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *realName;
@property (nullable, nonatomic, retain) NSString *username;
@property (nullable, nonatomic, retain) NSString *title;
@property (nullable, nonatomic, retain) NSString *id;
@property (nullable, nonatomic, retain) NSString *imageURL;
@property (nullable, nonatomic, retain) NSData *imageData;

@end

NS_ASSUME_NONNULL_END
