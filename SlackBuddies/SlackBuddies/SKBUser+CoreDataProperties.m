//
//  SKBUser+CoreDataProperties.m
//  SlackBuddies
//
//  Created by Marco Paiva on 2016-05-19.
//  Copyright © 2016 Marco Paiva. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "SKBUser+CoreDataProperties.h"

@implementation SKBUser (CoreDataProperties)

@dynamic realName;
@dynamic username;
@dynamic title;
@dynamic id;
@dynamic imageURL;
@dynamic imageData;

@end
