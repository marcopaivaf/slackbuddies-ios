//
//  SKBUser.h
//  SlackBuddies
//
//  Created by Marco Paiva on 2016-05-19.
//  Copyright © 2016 Marco Paiva. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface SKBUser : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "SKBUser+CoreDataProperties.h"
