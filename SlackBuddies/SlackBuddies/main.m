//
//  main.m
//  SlackBuddies
//
//  Created by Marco Paiva on 2016-05-15.
//  Copyright © 2016 Marco Paiva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SKBAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SKBAppDelegate class]));
    }
}
